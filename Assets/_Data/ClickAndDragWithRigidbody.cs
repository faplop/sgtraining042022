using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickAndDragWithRigidbody : MonoBehaviour
{
    public float moveSpeed = 2;

    public Rigidbody rb;



    private void Start()
    {
        /*rb = GetComponent<Rigidbody>();*/
        transform.Translate(0,0,2);
    }

    private void Update()
    {
        //transform.position += transform.forward * Time.deltaTime * moveSpeed;
        /*Transform camTransform = Camera.main.transform;
        
        Vector3 camPotision = new Vector3(camTransform.position.x, camTransform.position.y, camTransform.position.z);

        Vector3 direction = (transform.position - camPotision).normalized;
        
        Vector3 forwardMove = direction * Input.GetAxis("Vertical");

        Vector3 rightMove = camTransform.right * Input.GetAxis("Horizontal");

        Vector3 moverment = Vector3.ClampMagnitude(forwardMove + rightMove, 1);
        
        transform.Translate(moverment * moveSpeed * Time.deltaTime, Space.World);
        */

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(x,0,z);
        
        transform.Translate(movement * moveSpeed * Time.deltaTime);

    }
}
